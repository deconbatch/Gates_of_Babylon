/**
 * Gates of Babylon.
 * Some intuitively way to punch a hole on the layer with Processing.
 * 
 *
 * @author @deconbatch
 * @version 0.1
 * @license GPL Version 3 http://www.gnu.org/licenses/
 * Processing 3.5.3
 * 2021.01.09
 */

void setup() {
  size(980, 980);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  smooth();
  noLoop();
}

void draw() {
  int   ringRadius = 600;
  float noiseStep  = 0.006;
  float hueBase    = random(360.0);

  int   hW = floor(width * 0.5);
  int   hH = floor(height * 0.5);

  background(0.0, 0.0, 0.0, 100.0);
  chaos(hW, hH, ringRadius, noiseStep, hueBase);
  wall(width, height, ringRadius, hueBase);
  molding(hW, hH, ringRadius, hueBase);
  casing();
  
  saveFrame("frames/0001.png");
  exit();
}

/**
 * chaos : draw noise field
 */
private void chaos(int _centerX, int _centerY, int _fieldW, float _nStep, float _hue) {
  int halfW = floor(_fieldW * 0.5);

	noStroke();
	for (int x = _centerX - halfW; x < _centerX + halfW; x++) {
		float nX = x * _nStep;
		for (int y = _centerY - halfW; y < _centerY + halfW; y++) {
			float nY = y * _nStep;

			float nValA = noise(nX, nY, noise(nX, nY) * 5.0);
			float nValB = noise(nX, nY, noise(nX, nY) * 10.0);

			float nHue = (_hue + nValA * 180.0) % 360.0;
			float nSat = 30.0 + 60.0 * nValA;
			float nBri = 10.0 + 90.0 * nValB;
			float nAlp = 100.0;

			fill(nHue, nSat, nBri, nAlp);
			rect(x, y, 1.0, 1.0);
		}
	}
}

/**
 * wall : draw wall
 */
private void wall(int _w, int _h, float _holeR, float _hue) {
  // cobweb
  int lines = 18;
  float lDiv = _w * 1.6 / lines;
  float eSiz = lDiv * 0.4;
  PGraphics c = createGraphics(_w, _h);
  c.beginDraw();
  c.colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  c.background(0.0, 0.0, 0.0, 0.0);
  c.pushMatrix();
  c.translate(_w * 0.5, _h * 0.5);
  c.noFill();
  c.stroke(0.0, 0.0, 0.0, 100.0);
  c.strokeWeight(1.0);
  c.blendMode(BLEND);
  for (int i = 0; i < lines; i++) {
    float r = i * lDiv;
    c.rotate(TWO_PI / lines);
    c.line(0.0, 0.0, _w, 0.0);
    c.ellipse(0.0, 0.0, r, r);
  }
  c.blendMode(REPLACE);
  for (int i = 0; i < lines; i++) {
    c.rotate(TWO_PI / lines);
    for (int j = 0; j < 5; j++) {
      float x = floor(random(lines)) * lDiv;
      c.fill(0.0, 0.0, 50.0, 20.0);
      c.ellipse(x, 0.0, eSiz, eSiz);
      c.fill(0.0, 0.0, 0.0, 0.0);
      c.ellipse(x, 0.0, eSiz * 0.5, eSiz * 0.5);
    }
  }
  c.popMatrix();
  c.endDraw();

  // noise field
  PGraphics w = createGraphics(_w, _h);
  w.beginDraw();
  w.colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  w.blendMode(BLEND);
  w.background((_hue + 90.0) % 360.0, 40.0, 30.0, 100.0);

  float nStep = 0.02;
  w.noStroke();
	for (int x = 0; x < _w; x++) {
		float nX = x * nStep;
		for (int y = 0; y < _h; y++) {
			float nY = y * nStep;

			float nValA = noise(nX, nY, noise(nX, nY) * 5.0);
			float nValB = noise(nX, nY, noise(nX, nY) * 10.0);

			float nHue = (_hue + 90.0) % 360.0;
			float nSat = 20.0 + 40.0 * nValA;
			float nBri = 10.0 + 40.0 * nValB;
			float nAlp = 100.0;

			w.fill(nHue, nSat, nBri, nAlp);
			w.rect(x, y, 1.0, 1.0);
		}
	}

  // draw cobweb on the wall
  w.image(c, 0, 0);

  // punch a hole on the wall
  w.blendMode(REPLACE);
  w.fill(0.0, 0.0, 0.0, 0.0);
  w.noStroke();
  w.ellipse(_w * 0.5, _h * 0.5, _holeR, _holeR);
  w.endDraw();

  image(w, 0, 0);
}

/**
 * molding : draw molding
 */
private void molding(int _centerX, int _centerY, float _holeR, float _hue) {
  int lines = 2;
  int waves = 5;
  pushMatrix();
  translate(_centerX, _centerY);
  noFill();
  // inner cirlce
  stroke((_hue + 120.0) % 360.0, 40.0, 50.0, 100.0);
  strokeWeight(20.0);
  ellipse(0.0, 0.0, _holeR, _holeR);
  // outer circle
  stroke((_hue + 120.0) % 360.0, 50.0, 60.0, 100.0);
  strokeWeight(30.0);
  ellipse(0.0, 0.0, _holeR + 40.0, _holeR + 40.0);
  // curve lines
  stroke(0.0, 0.0, 0.0, 100.0);
  strokeWeight(1.0);
  for (int l = 0; l < lines; l++) {
    beginShape();
    for (float t = 0.0; t < TWO_PI; t += PI * 0.01) {
      float r = _holeR * 0.5 + 20.0 + 10.0 * sin(sin(t * waves + l * TWO_PI / lines) * TWO_PI);
      float x = r * cos(t);
      float y = r * sin(t);
      curveVertex(x, y);
    }
    endShape(CLOSE);
  }
  popMatrix();
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
  noStroke();
}
